from .base import *

DEBUG = True

ALLOWED_HOSTS = ['ip-address', 'wwww.mywebsite.com']


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

STRIPE_PUBLIC_KEY = 'YOUR-LIVE-PUBLIC-KEY'
STRIPE_SECRET_KEY = 'YOUR_LIVE_SECRET_KEY'